The base role for managing the docker fleet on AWS and Google Cloud.

Includes two extra playbooks for creating an all-new EC2 host from scratch
or reprovisioning the existing hosts.

Has a few dependencies not included here:

* set_cloud - set cloud* variables at runtime
* access - a basic user directory
* gcloud - Google Cloud tooling

Written in 2015.
